<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
    <link href="./css/custom.css" rel="stylesheet" type="text/css">
    <link href="./css/slider.css" rel="stylesheet" type="text/css">

    <script>
        $(document).ready(function(){
          $(window).scroll(function() { // check if scroll event happened
            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
              $(".navbar-fixed-top").css("background-color", "#FFFFF8")
                .css("border-bottom","1px solid #f2f2f2"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
              $(".nav > li > a").css("color","#000").css("text-shadow","0px 0px 0px #000000");
              $("a.navbar-brand").css("color","#000").css("text-shadow","0px 0px 0px #000000");
            } else {
              $(".navbar-fixed-top").css("background-color", "transparent")
                .css("border-bottom","0px solid #f2f2f2"); // if not, change it back to transparent
              $(".nav > li > a").css("color","#fff").css("text-shadow","1px 1px 1px #000000");
              $("a.navbar-brand").css("color","#fff").css("text-shadow","1px 1px 1px #000000");
            }
          });
        });
    </script>

  </head>
  <body>
    <div class="navbar navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><b><span>Ilalang Tour &amp; Travel</span><br></b><br></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a href="#">Home</a>
            </li>
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle">Paket Liburan <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="#">2 Hari 1 Malam</a></li>
                  <li class="divider"></li>
                  <li><a href="#">3 Hari 2 Malam</a></li>
                  <li class="divider"></li>
                  <li><a href="#">4 Hari 3 Malam</a></li>
              </ul>
            </li>
            <li>
              <a href="#">Info Belitong</a>
            </li>
            <li>
              <a href="#">Tentang Kami</a>
            </li>
            <li>
              <a href="#">Galeri<br></a>
            </li>
            <li>
              <a href="#">Kontak Kami<br></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="cover">
      <!-- <div class="cover-image" style="background-image : url('img/pantai-nyiur-melambai.jpeg')"></div> -->
      <div class="cover-image">
        <div id="first-slider">
          <div id="carousel-example-generic" class="carousel slide carousel-fade">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="3"></li>
              </ol>
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  <!-- Item 1 -->
                  <div class="item active slide1">
                      <div class="row"><div class="container">
                          <div class="col-md-3 text-right">
                              <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/pfmmo6qj1/window_domain.png">
                          </div>
                          <div class="col-md-9 text-left">
                              <h3 data-animation="animated bounceInDown">Add images, or even your logo!</h3>
                              <h4 data-animation="animated bounceInUp">Easily use stunning effects</h4>
                              <a class="btn btn-lg btn-primary">Check Paket Liburan</a>
                           </div>
                      </div></div>
                   </div>
                  <!-- Item 2 -->
                  <div class="item slide2">
                      <div class="row"><div class="container">
                          <div class="col-md-7 text-left">
                              <h3 data-animation="animated bounceInDown"> 50 animation options A beautiful</h3>
                              <h4 data-animation="animated bounceInUp">Create beautiful slideshows </h4>
                              <a class="btn btn-lg btn-primary">Check Paket Liburan</a>
                           </div>
                          <div class="col-md-5 text-right">
                              <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/sp11uneml/rack_server_unlock.png">
                          </div>
                      </div></div>
                  </div>
                  <!-- Item 3 -->
                  <div class="item slide3">
                      <div class="row"><div class="container">
                          <div class="col-md-7 text-left">
                              <h3 data-animation="animated bounceInDown">Simple Bootstrap Carousel</h3>
                              <h4 data-animation="animated bounceInUp">Bootstrap Image Carousel Slider with Animate.css</h4>
                              <a class="btn btn-lg btn-primary">Check Paket Liburan</a>
                           </div>
                          <div class="col-md-5 text-right">
                              <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/eq8xvxeq5/globe_network.png">
                          </div>
                      </div></div>
                  </div>
                  <!-- Item 4 -->
                  <div class="item slide4">
                      <div class="row"><div class="container">
                          <div class="col-md-7 text-left">
                              <h3 data-animation="animated bounceInDown">We are creative</h3>
                              <h4 data-animation="animated bounceInUp">Get start your next awesome project</h4>
                              <a class="btn btn-lg btn-primary">Check Paket Liburan</a>
                           </div>
                          <div class="col-md-5 text-right">
                              <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/9vf8xngel/internet_speed.png">
                          </div>
                      </div></div>
                  </div>
                  <!-- End Item 4 -->

              </div>
              <!-- End Wrapper for slides-->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
              </a>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="./img/placeholder.png" class="img-responsive">
          </div>
          <div class="col-md-6">
            <h1 class="text-primary">Paket 2 Hari 1 Malam
              <br>
            </h1>
            <p class="lead txt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
              ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
              dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
              nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
              enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
              felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
              elementum semper nisi.</p>
              <button class="btn btn-primary">Lihat Selengkapnya</button>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="text-primary">Paket 3 Hari 2 Malam
              <br>
            </h1>
            <p class="lead txt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
              ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
              dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
              nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
              enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
              felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
              elementum semper nisi.</p>
              <button class="btn btn-primary">Lihat Selengkapnya</button>
          </div>
          <div class="col-md-6">
            <img src="./img/placeholder.png" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="./img/placeholder.png" class="img-responsive">
          </div>
          <div class="col-md-6">
            <h1 class="text-primary">Paket 4 Hari 3 Malam</h1>
            <p class="lead txt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
              ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
              dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
              nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
              enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
              felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
              elementum semper nisi.</p>
              <button class="btn btn-primary">Lihat Selengkapnya</button>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center text-primary">Tentang Kami</h1>
            <p class="text-center lead">siapa kami ? dan layanan apa yang akan kami berikan&nbsp;</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img src="./img/placeholder.png" class="img-responsive">
          </div>
          <div class="col-md-8">
            <p class="lead txt">Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud&nbsp;Lorem ipsum dolor sit amet,
              consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
              massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
              ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
              quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
              vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
              a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.
              Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center text-primary">Gallery</h1>
            <p class="text-center lead">Tempat Wisata di Pulau Belitong</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
          <div class="col-md-3">
            <a><img src="./img/placeholder.png" class="img-responsive"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center text-primary">Kontak Kami
              <br>
            </h1>
            <p class="text-center" style="font-size:18px;"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-3">
              <img src="./img/placeholder.png" class="img-responsive">
            </div>
            <div class="col-sm-8">
              <form id="contact-form" method="post" action="contact.php" role="form">
                <div class="messages"></div>
                <div class="controls">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">Firstname *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Lastname *</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_phone">Phone</label>
                                <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-send" value="Send message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted"><strong>*</strong> Pesan Berhasil</p>
                        </div>
                    </div>
                </div>

            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="section" style="border-top:2px solid #f2f2f2;">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1>Ilalang Tour &amp; Travel</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
          <div class="col-sm-6">
            <p class="text-info text-right">
              <br>
              <br>
            </p>
            <div class="row">
              <!-- <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div> -->
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-right">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-decoration"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
</body>
</html>
